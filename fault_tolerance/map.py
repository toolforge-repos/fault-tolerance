from collections import defaultdict, OrderedDict

def handle_map(cluster, clusters, netbox_config):
    if not cluster or cluster not in clusters.keys():
        return {'map': None, 'rows': None}
    hosts_in_cluster = [i.split('.')[0] for i in clusters[cluster]]
    rack_to_host_map = defaultdict(list)
    dcs_having_this_cluster = []
    for host in netbox_config['results']:
        if host['name'] in hosts_in_cluster and host['rack'] and host['rack'].get('name'):
            rack_to_host_map['{}:{}'.format(host['site']['slug'], host['rack']['name'])].append(host['name'])
            if host['site']['slug'] not in dcs_having_this_cluster:
                dcs_having_this_cluster.append(host['site']['slug'])

    map_ = defaultdict(dict)
    for host in netbox_config['results']:
        if host['rack'] and host['site']:
            dc = host['site']['slug']
            if dc not in dcs_having_this_cluster:
                continue
            if host['rack']['name'] not in map_[dc]:
                map_[dc][host['rack']['name']] = map_[dc].get(host['rack']['name'], []) + rack_to_host_map.get('{}:{}'.format(host['site']['slug'], host['rack']['name']), [])
    
    for dc in map_:
        map_[dc] = OrderedDict(sorted(map_[dc].items()))
    rows = {}
    for dc in map_:
        rows[dc] = defaultdict(list)
        for rack in map_[dc]:
            rows[dc][rack[0]].append(rack)
        rows[dc] = OrderedDict(sorted(rows[dc].items()))
    return {'map': map_, 'rows': rows}
