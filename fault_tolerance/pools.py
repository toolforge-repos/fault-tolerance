from collections import OrderedDict, defaultdict
import math

import requests

from .clusters import DCs


def handle_pools():
    res = requests.get('https://config-master.wikimedia.org/pools.json').json()
    final_res = OrderedDict()
    for dc in DCs:
        per_dc_weights_pooled = defaultdict(int)
        per_dc_weights_total = defaultdict(int)
        per_dc_hosts = defaultdict(int)
        for cluster in res.get(dc, {}):
            for host in res[dc][cluster]:
                for service in res[dc][cluster][host]:
                    service_data = res[dc][cluster][host][service]
                    if service_data['pooled']:
                        per_dc_weights_pooled[cluster + '---' + service] += service_data['weight']
                    per_dc_weights_total[cluster + '---' + service] += service_data['weight']
                    per_dc_hosts[cluster + '---' + service] += 1
        total_hosts = sum(per_dc_hosts.values())
        per_dc_final_res = OrderedDict()
        gridstart = 1
        service_sizes = {}
        for service in sorted(per_dc_hosts.keys()):
            service_sizes[service] = int(math.sqrt(per_dc_hosts[service]*100/total_hosts))
        for service in OrderedDict(sorted(service_sizes.items(), key=lambda t: t[1], reverse=True)):
            if not per_dc_weights_total[service]:
                continue
            percent = (per_dc_weights_pooled[service]*100)/per_dc_weights_total[service]
            if percent > 90:
                color = '#00af89'
            elif percent > 70:
                color = '#d5fdf4'
            elif percent > 50:
                color = '#fc3'
            else:
                color = '#d33'
            size = int(math.sqrt(per_dc_hosts[service]*100/total_hosts))
            if size == 0:
                continue
            if gridstart + size > 6:
                gridstart = 1
            per_dc_final_res[service] = {
                'percent_pooled': int(percent),
                'hosts':  per_dc_hosts[service],
                'color': color,
                'gridstart': gridstart,
                'gridend': gridstart + size,
                'height': size * 75,
                'size': size,
                'cluster': service.split('---')[0],
                'service': service.split('---')[1],
                'fontsize': 0.8 + (size * 0.25),
            }
            gridstart = (gridstart + size) % 6
            if gridstart == 0:
                gridstart = 1
        final_res[dc] = per_dc_final_res
    return final_res

def handle_pools_table():
    res = requests.get('https://config-master.wikimedia.org/pools.json').json()
    final_res = OrderedDict()
    for dc in DCs:
        per_dc_weights_pooled = defaultdict(int)
        per_dc_weights_total = defaultdict(int)
        per_dc_hosts_pooled = defaultdict(int)
        per_dc_hosts = defaultdict(int)
        for cluster in res.get(dc, {}):
            for host in res[dc][cluster]:
                for service in res[dc][cluster][host]:
                    service_data = res[dc][cluster][host][service]
                    if service_data['pooled']:
                        per_dc_weights_pooled[cluster + '---' + service] += service_data['weight']
                        per_dc_hosts_pooled[cluster + '---' + service] += 1
                    per_dc_weights_total[cluster + '---' + service] += service_data['weight']
                    per_dc_hosts[cluster + '---' + service] += 1
        per_dc_final_res = OrderedDict()
        for service in OrderedDict(sorted(per_dc_hosts.items(), key=lambda t: t[1], reverse=True)):
            if not per_dc_weights_total[service]:
                continue
            percent = (per_dc_weights_pooled[service]*100)/per_dc_weights_total[service]
            if percent > 90:
                color = '#d5fdf4'
            elif percent > 70:
                color = '#eaf3ff'
            elif percent > 50:
                color = '#fef6e7'
            else:
                color = '#fee7e6'
            per_dc_final_res[service] = {
                'percent_pooled': int(percent),
                'pooled': per_dc_hosts_pooled[service],
                'hosts':  per_dc_hosts[service],
                'cluster': service.split('---')[0],
                'service': service.split('---')[1],
                'color': color,
            }
        final_res[dc] = per_dc_final_res
    return final_res
