
import requests
from pathlib import Path
import os
from collections import defaultdict, OrderedDict

DCs = ['eqiad', 'codfw', 'esams', 'ulsfo', 'eqsin', 'drmrs']

class ClusterBuilder():

    def get_clusters_from_pybal(self):
        res = requests.get('https://config-master.wikimedia.org/pools.json').json()
        clusters = defaultdict(list)
        for dc in res:
            for cluster in res[dc]:
                hosts = [i.split('.')[0] for i in res[dc][cluster] if not i.startswith('dbproxy')]
                if hosts:
                    clusters[cluster] += hosts
        return clusters

    def get_clusters_from_puppet(self, config):
        clusters = {}
        for cluster in config['clusters']:
            hosts = []
            for path in Path(os.path.join(config['puppet_path'], 'hieradata', 'hosts')).rglob( config['clusters'][cluster].get('hostprefix', '') + '*.yaml'):
                with open(path, 'r') as f:
                    if config['clusters'][cluster]['hieracontent'] in f.read():
                        hosts.append(path.name.replace('.yaml', ''))
            clusters[cluster] = hosts
        return clusters
    
    def get_clusters_from_dbctl(self):
        clusters = defaultdict(list)
        for dc in ['eqiad', 'codfw']:
            dc_info = requests.get('https://noc.wikimedia.org/dbconfig/{}.json'.format(dc)).json()
            for cluster in dc_info['externalLoads']:
                cluster_info = dc_info['externalLoads'][cluster]
                hosts = cluster_info[0]
                hosts.update(cluster_info[1])
                clusters[cluster] += [i.split(':')[0] for i in hosts]
            for cluster in dc_info['sectionLoads']:
                cluster_info = dc_info['sectionLoads'][cluster]
                hosts = cluster_info[0]
                hosts.update(cluster_info[1])
                if cluster == 'DEFAULT':
                    cluster = 's3'
                clusters[cluster] += [i.split(':')[0] for i in hosts]
        return clusters


class Clusters():
    def build(self, config):
        self.cluster_flags = defaultdict(dict)
        builder = ClusterBuilder()
        self.clusters = builder.get_clusters_from_pybal()
        self.clusters.update(builder.get_clusters_from_puppet(config))
        dbctl_clusters = builder.get_clusters_from_dbctl()
        self.clusters.update(dbctl_clusters)
        self.clusters = OrderedDict(sorted(self.clusters.items()))
        for cluster in dbctl_clusters:
            master = list(set(dbctl_clusters[cluster]).intersection(self.clusters['db-masters']))
            candidate_master = list(set(dbctl_clusters[cluster]).intersection(self.clusters['db-master-candidates']))
            if master:
                for master_in_dc in master:
                    self.cluster_flags[cluster][master_in_dc] = 'master'
                    self.cluster_flags['db-masters'][master_in_dc] = cluster
            
            if candidate_master:
                for candidate_master_in_dc in candidate_master:
                    self.cluster_flags[cluster][candidate_master_in_dc] = 'candidate-master'
                    self.cluster_flags['db-master-candidates'][candidate_master_in_dc] = cluster

