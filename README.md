# SwitchMaster
Map of fault domains and criticality of them per service cluster.

## Installing dependencies
This application uses Python 3 and Flask. Using a virtual environment is recommended:

```
# Unix-based systems
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# Windows systems
python3 -m venv venv
venv\Scripts\activate
pip install -r requirements.txt
```

## Create a basic config
Create a config.yaml like this:
```
puppet_path: '/home/amir/puppet/'
clusters:
  db-masters:
    hostprefix: db
    hieracontent: "profile::mariadb::mysql_role: 'master'"
  db-master-candidates:
      hostprefix: db
      hieracontent: "candidate master"
```

And then create a netbox json file. Like this:
```
[{
  "name": "alert1001",
  "rack": {
    "id": 22,
    "url": "https://netbox.wikimedia.org/api/dcim/racks/22/?format=json",
    "display": "C6",
    "name": "C6"
  },
  ...
]
```

(The response can be obtained from https://netbox.wikimedia.org/api/dcim/devices/?format=json&limit=1000&role_id=1)

Note that you need a valid API token for this. To obtain:

* Open your profile page on https://netbox.wikimedia.org/
* Click on "API Tokens" and Add a new token with a sane expiration and description
* Once the secret key has been generated, use it to obtain the json with a request like:

```
curl \
  -H "Authorization: Token  <YOUR_TOKEN_HERE>" \
  -H "Accept: application/json; indent=4" \
  'https://netbox.wikimedia.org/api/dcim/devices/?format=json&limit=1000&role_id=1' \
  -o netbox.json
```

* Note that you can do the same using the Makefile:

```
export NETBOX_TOKEN=<YOUR_TOKEN_HERE> make download-netbox-data
```

## Starting the application
You can use the `flask` command line utility to start the app:

```
# Unix-based systems
source venv/bin/activate
export FLASK_ENV=development
export FLASK_APP=app
flask run

# Or you could use Makefile:
make run

# Windows systems
venv\Scripts\activate
set FLASK_ENV=development
set FLASK_APP=app
flask run
```
