FLASK_APP = app
FLASK := FLASK_APP=$(FLASK_APP) ./venv/bin/flask
NETBOX_TOKEN ?= changeme
NETBOX_API_URL = 'https://netbox.wikimedia.org/api/dcim/devices/?format=json&limit=0&role_id=1'
NETBOX_JSON_FILE = netbox.json

download-netbox-data:
	curl -H "Authorization: Token $(NETBOX_TOKEN)" -H "Accept: application/json; indent=4" $(NETBOX_API_URL) -o $(NETBOX_JSON_FILE)

run:
	FLASK_ENV=development $(FLASK) run
