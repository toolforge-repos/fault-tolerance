import re
from flask import Flask, render_template, request
import os
import json
import yaml

from fault_tolerance import map
from fault_tolerance.pools import handle_pools, handle_pools_table
from fault_tolerance.clusters import Clusters, DCs
app = Flask(__name__)
dir = os.path.dirname(os.path.abspath(__file__))

with open(os.path.join(dir, 'config.yaml'), 'r') as f:
    config = yaml.safe_load(f)

# TODO: This needs to be more automated
with open(os.path.join(dir, 'netbox.json'), 'r') as f:
    netbox_config = json.loads(f.read())

clusters = Clusters()
@app.route("/")
def hello():
    return render_template('home.html')


@app.route("/map", methods=['GET'])
def map_route():
    clusters.build(config)
    map_ = map.handle_map(request.args.get('cluster'), clusters.clusters, netbox_config)
    
    return render_template(
        'map.html',
        map=map_['map'],
        rows=map_['rows'],
        cluster=request.args.get('cluster'),
        cluster_flags=clusters.cluster_flags,
        dcs=DCs,
        clusters=clusters.clusters)

@app.route("/pools", methods=['GET'])
def pools():
    return render_template(
        'pools.html',
        res=handle_pools()
    )
@app.route("/pools/table", methods=['GET'])
def pools_table():
    return render_template(
        'pools_table.html',
        res=handle_pools_table()
    )

if __name__ == "__main__":
    app.run(host='0.0.0.0')

