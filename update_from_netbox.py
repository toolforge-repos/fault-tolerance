import requests
import json
import os

res = requests.get('https://netbox.wikimedia.org/api/dcim/devices/?format=json&limit=0&role_id=1',
    headers={'Authorization': 'Token ' + os.getenv('NETBOX_TOKEN')}).json()

final_res = {'results' : []}

while res.get('next'):
    for case in res['results']:
        final_res['results'].append({
            'name': case['name'],
            'rack': case['rack'],
            'site': case['site']
        })
    
    res = requests.get(res['next'], headers={'Authorization': 'Token ' + os.getenv('NETBOX_TOKEN')}).json()

with open('netbox.json', 'w') as f:
    f.write(json.dumps(final_res, indent='\t'))